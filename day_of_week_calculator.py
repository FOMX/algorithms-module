"""
some sort of day o fhte week calculator
by todd

"""
import datetime


def day_of_week_from_datetime(year, month, day):
    # day of week from datetime
    date = datetime.datetime(year=year, month=month, day=day)
    return date.weekday()


def day_of_week_from_Zeller(year, month, day):
    """ Zeller's congruence
    https://en.wikipedia.org/wiki/Zeller%27s_congruence
    """
    century = year // 100
    year = year % 100
    month = (month-2) % 12 + 2 # because january and february are 13 and 14 in this alg
    day_of_week = (day + (13*(month+1))//5 + year + year//4 + century // 4 + 5*century ) % 7
    day_of_week = (day_of_week + 5) % 7 # correcting for day
    return day_of_week

def main():
    day = 1
    month = 7
    year = 1999
    print(day_of_week_from_datetime(year=year, month=month, day=day))
    print(day_of_week_from_Zeller(year=year, month=month, day=day))


if __name__=='__main__':
    main()
