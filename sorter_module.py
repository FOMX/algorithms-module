"""
Sorting module

- quick sort
- tim sort (a.k.a pythons native sort)
- heap sort
- merge sort
- bubble sort
- selection sort
- insertion sort
"""

TEST_LIST = [1, 4, 1, 2, 5, 1, 2, 1, 4]


# todo add verbose params for outputting step by step

def insertionsort(arr):
    """
    insertion sort
    :param arr:
    :return: sorted array

    sorts in place
    post grad

    """

    def _insertionsort(arr):
        i = 0
        while i < len(arr):
            j = i
            while j > 0 and arr[j-1] > arr[j]:
                arr[j-1], arr[j] = arr[j], arr[j-1]
                j -= 1
            i += 1


        return arr

    return _insertionsort(arr)


def selectionsort(arr):
    """
    selection sort
    :param arr:
    :return: sorted array

    sorts in place

    for j = 0 to len(arr)
        iteration to find the jth smallest value
        min_index = j = (index of element)
        min_val = test_element = arr(j)

        for i = j+1 to len(arr)
            scan through the array from j + 1 to the end
            next_element = arr[i]

            if next_element < min_val
                store the smallest value and its index
                min_index = i
                min_val = next_element

        after scanning, swap the jth smallest value with the element in the jth location.
        arr[min_index], arr[j] = arr[j], arr[min_index]

        continue onto the j+1th smallest value

    """

    def _selectionsort(arr):
        for j in range(len(arr)):
            min_index = j
            min_val = arr[j]
            for i in range(j+1, len(arr)):
                if arr[i] < min_val:
                    min_index = i
                    min_val = arr[i]

            if min_index != j:
                arr[j], arr[min_index] = arr[min_index], arr[j]

        return arr

    return _selectionsort(arr)

def bubblesort(arr):
    """
    bloody bubblesort
    :param arr:
    :return: sorted arr

    flag swapped = True
    while swapped == True
        reset flag swapped = False
        scan the array 2 elements at a time
        if arr[i+1] < arr[i]:
            swap i and i+1
            swapped = True
    """

    def _bubblesort(arr_):
        """
        :param sub_arr:
        :return:
        """
        swapped = True
        comparisons = len(arr_) - 1
        while swapped:
            swapped = False
            for i in range(comparisons):
                if arr[i + 1] < arr[i]:
                    arr[i + 1], arr[i] = arr[i], arr[i + 1]
                    swapped = True

        return arr_

    return _bubblesort(arr)


def quicksort(arr):
    """
    :param arr:
    :return: sorted arr

    get median value
    partition array about the median value
    left = quicksort(left partition)
    right = quicksort(right partition)
    return left.append(right)

    """

    def _median_index(sub_arr):
        """ returns the index of the median value
        :param sub_arr: sub array to sort
        :return: index of pseudo median

        psuedo median
        median of first, last and middle value

        """
        indices = [0, (len(sub_arr) - 1) // 2, len(sub_arr) - 1]
        new_arr = [(ind, sub_arr[ind]) for ind in indices]
        new_arr.sort(key=lambda x: x[1])
        return new_arr[1][0]

    def _quicksort(sub_arr):
        """
        recursive quick sort
        :param sub_arr:
        :return: sorted(arr)
        """

        if len(sub_arr) <= 2:
            return sorted(sub_arr)
        else:
            pivot_index = _median_index(sub_arr)
            left, right = _partition(sub_arr, pivot_index)
            left = _quicksort(left)
            right = _quicksort(right)
            return left + right

    def _partition(sub_arr, pivot_index):
        """
        partitions sub array into left and right partitions
        :param sub_arr:
        :param pivot_index:
        :return:
        """
        left_partition = []
        right_partition = []
        pivot = sub_arr[pivot_index]
        flip = True
        for val in sub_arr:
            if val < pivot:
                left_partition.append(val)
            elif val > pivot:
                right_partition.append(val)
            else:
                if flip:
                    left_partition.append(val)
                    flip = False
                else:
                    right_partition.append(val)
                    flip = True
        return left_partition, right_partition

    return _quicksort(arr)


def quicksort_inplace(arr):
    """
    :param arr:
    :return: sorted arr
    """
    pass


def mergesort(arr):
    """ merge sort
    :param arr:
    :return: sorted arr

    if len(arr)<2 sort arr
    else
        pick a pivot (median element)
        split array in 2 about pivot
        mergesort left
        mergesort right
        merge left and right
        return joined arr
    """

    def _mergesort(arr):
        """ merge method
            time complexity O(nlogn)
        """
        n = len(arr)
        if n <= 2:
            return sorted(arr)
        else:
            p = n // 2
            left = _mergesort(arr[:p])
            right = _mergesort(arr[p:])
            return merge(left, right)

    def merge(a, b):
        """ merge method
        time complexity O(n)
        compare each element 1 by 1 and stack them

        todo this could be improved
        """
        imax, jmax = len(a), len(b)
        kmax = len(a) + len(b) - 1
        c = []
        i = j = k = 0
        while k < kmax and i < imax and j < jmax:
            if a[i] < b[j]:
                c.append(a[i])
                i += 1
                k += 1
            elif a[i] > b[j]:
                c.append(b[j])
                j += 1
                k += 1
            else:
                c.append(a[i])
                c.append(b[j])
                i += 1
                j += 1
                k += 2

        for m in range(i, imax):
            c.append(a[m])

        for m in range(j, jmax):
            c.append(b[m])

        return c

    if len(arr) <= 1:
        return arr
    else:
        arr_sorted = _mergesort(arr)
        return arr_sorted


def main():
    tim_result = sorted(TEST_LIST)
    merge_result = mergesort(TEST_LIST[:])
    quicksort_result = quicksort(TEST_LIST[:])
    bubblesort_result = bubblesort(TEST_LIST[:])
    selectionsort_result = selectionsort(TEST_LIST[:])
    insertionsort_result = insertionsort(TEST_LIST[:])
    # assert(merge_result==tim_result)
    print(merge_result)
    print(tim_result)
    print(quicksort_result)
    print(bubblesort_result)
    print(selectionsort_result)
    print(insertionsort_result)


if __name__ == '__main__':
    main()
