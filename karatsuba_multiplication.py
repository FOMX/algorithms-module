"""
karatsuba multiplication

"""
# integer multiplication

# katsuba algorithm
import time
import timeit
from math import log10


def timed_function(fun):
    def wrapped(*args, **kwargs):
        _s = time.time()
        result = fun(*args, **kwargs)
        _f = time.time()
        print('fun:{} finished in {} seconds'.format(fun.__name__, _f - _s))
        return result

    return wrapped


# @timed_function
def karatsuba(x, y):
    """
    :param x:
    :param y:
    :return: x*y
    """
    def k_mult(x, y):

        if x < 10 and y < 10:
            return mult_table[x][y]
        else:
            n = max(len(str(x)), len(str(y)))

            ten_n_2 = 10 ** (n // 2)

            a = x // ten_n_2
            b = x % ten_n_2
            c = y // ten_n_2
            d = y % ten_n_2

            ac = k_mult(a, c)
            bd = k_mult(b, d)
            ad_bd = k_mult(a + b, c + d) - ac - bd

            return ac * ten_n_2 ** 2 + (ad_bd * ten_n_2) + bd

    return k_mult(x, y)


# @timed_function
def karatsuba2(x, y):
    def k_mult(x, y):

        if x < 10 and y < 10:
            return mult_table[x][y]
        else:
            n = max(len(str(x)), len(str(y)))
            # = max(int(log10(x+0.01))+1, int(log10(y+0.01))+1)

            ten_n_2 = 10 ** (n // 2)

            a, b = divmod(x, ten_n_2)
            c, d = divmod(y, ten_n_2)

            ac = k_mult(a, c)
            bd = k_mult(b, d)
            ad_bd = k_mult(a + b, c + d) - ac - bd

            return ac * ten_n_2 ** 2 + (ad_bd * ten_n_2) + bd

    return k_mult(x, y)


x = 3141592653589793238462643383279502884197169399375105820974944592
y = 2718281828459045235360287471352662497757247093699959574966967627

mult_table = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
              [0, 2, 4, 6, 8, 10, 12, 14, 16, 18],
              [0, 3, 6, 9, 12, 15, 18, 21, 24, 27],
              [0, 4, 8, 12, 16, 20, 24, 28, 32, 36],
              [0, 5, 10, 15, 20, 25, 30, 35, 40, 45],
              [0, 6, 12, 18, 24, 30, 36, 42, 48, 54],
              [0, 7, 14, 21, 28, 35, 42, 49, 56, 63],
              [0, 8, 16, 24, 32, 40, 48, 56, 64, 72],
              [0, 9, 18, 27, 36, 45, 54, 63, 72, 81]
              ]

kar_1 = karatsuba(x, y)
kar_2 = karatsuba2(x, y)
mult = x * y

# why do i have to do this omg
# something is wrong with the timeit module
import builtins
builtins.__dict__.update(locals())

repeats = 3
iters = 1000
s = 'karatsuba(x, y)'
t = timeit.Timer(stmt=s)
print(t.repeat(repeats, iters))

s = 'karatsuba2(x, y)'
t = timeit.Timer(stmt=s)
print(t.repeat(repeats, iters))
print(kar_1 == kar_2 == mult)
